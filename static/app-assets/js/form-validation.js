/*=========================================================================================
	File Name: form-validation.js
	Description: jquery bootsreap validation js
	----------------------------------------------------------------------------------------
	Item Name: APM - Responsive Admin Theme
	Version: 2.1
	Author: RTG
	Author URL: http://www.themeforest.net/user/RTG
==========================================================================================*/

(function(window, document, $) {
	'use strict';

	// Input, Select, Textarea validations except submit button
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

})(window, document, jQuery);