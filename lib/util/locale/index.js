import _ from 'lodash'

const mapping = {
  en: 'English',
  fr: 'Français',
  es: 'Español'
}

/**
 * Return the display text for each local code. en = English
 * Default is english
 * @param {*} lang - locale code (eg. en, fr)
 */
function getDisplay(lang) {
  return _.get(mapping, lang, 'English')
}

const locales = ['en', 'fr', 'es']

export { getDisplay, locales, mapping }
