
module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'AMP Appraisal Management Platform',
    meta: [
      { charset: 'utf-8' },
      { content: 'text/html; charset=UTF-8', 'http-equiv': 'Content-Type' },
      { content: 'IE=edge', 'http-equiv': 'X-UA-Compatible' },
      {
        name: 'viewport',
        content:
          'width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui'
      },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900'
      },
      {
        rel: 'stylesheet',
        href:
          'https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css'
      }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    // BEGIN VENDOR CSS
    '~/static/app-assets/fonts/feather/style.min.css',
    '~/static/app-assets/fonts/simple-line-icons/style.css',
    '~/static/app-assets/fonts/font-awesome/css/font-awesome.min.css',
    '~/static/app-assets/vendors/css/perfect-scrollbar.min.css',
    '~/static/app-assets/vendors/css/prism.min.css',
    '~/static/app-assets/vendors/css/chartist.min.css',
    '~/static/app-assets/vendors/css/fullcalendar.min.css',
    // END VENDOR CSS
    '~/static/app-assets/css/app.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~plugins/vue-full-calendar', ssr: false }
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    ['@nuxtjs/axios'],
    ['@nuxtjs/apollo']
  ],
  // Give apollo module options
  apollo: {
    tokenName: 'yourApolloTokenName', // optional, default: apollo-token
    tokenExpires: 10, // optional, default: 7 (days)
    includeNodeModules: true, // optional, default: false (this includes graphql-tag for node_modules folder)
    authenticationType: 'Basic', // optional, default: 'Bearer'
    // (Optional) Default 'apollo' definition
    defaultOptions: {
      // See 'apollo' definition
      // For example: default query options
      $query: {
        loadingKey: 'loading',
        fetchPolicy: 'cache-and-network',
      },
    },
    // required
    clientConfigs: {
      default: {
        // required  
        httpEndpoint: 'http://localhost:4000',
        // optional
        // See https://www.apollographql.com/docs/link/links/http.html#options
        httpLinkOptions: {
          credentials: 'same-origin'
        },
        // You can use `wss` for secure connection (recommended in production)
        // Use `null` to disable subscriptions
        wsEndpoint: null, // optional
        // LocalStorage token
        tokenName: 'apollo-token', // optional
        // Enable Automatic Query persisting with Apollo Engine
        persisting: false, // Optional
        // Use websockets for everything (no HTTP)
        // You need to pass a `wsEndpoint` for this to work
        websocketsOnly: false // Optional
      }
    }
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
